Write-Host "Killig SPA on 8080..." -ForegroundColor DarkGreen
kill-port 8080

Write-Host "Killig all nginx processes..." -ForegroundColor DarkGreen
taskkill /f /IM nginx.exe
kill-port 8081

Write-Host "Killing php-cgi on 8099..." -ForegroundColor DarkGreen
kill-port 8099

Write-Host "Killig Brotcast on 8086..." -ForegroundColor DarkGreen
kill-port 8086

Write-Host "Killig Websockets on 6001..." -ForegroundColor DarkGreen
kill-port 6001

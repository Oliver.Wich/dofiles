function isInstalled {
	param (
		[Parameter(Mandatory=$true, Position=0)]
        [string]$Name
    )
	$out = winget list $Name
	return !($out -Match "kein installiertes")
}

function install {
	param (
		[Parameter(Mandatory=$true, Position=0)]
        [string]$Name
    )

	Write-Host "Installing Package $Name with WinGet..."
	winget install -e --id $Name
}

function installIfNotInstalled {
	param (
		[Parameter(Mandatory=$true, Position=0)]
        [string]$Name,
		[Parameter(Position=1)]
        [string]$CommonName
    )

	# CommonName is for applications that can be installed from elsewhere. Use this only when necessary as it can slow down the installation process considerably
	if (!$CommonName) {
		$CommonName = $Name
	}

	Write-Host "Trying to install $CommonName..."
	if (isInstalled $CommonName) {
		Write-Host "$CommonName is already installed." -ForegroundColor DarkRed
	} else {
		Write-Host "Installing $CommonName..." -ForegroundColor Cyan
		install $Name
	}
}

function installWSL {
	$wslOut = wsl --help # We do not care about the output, but we want to supress it
	# WSL is not installed if $LASTEXITCODE is NOT -1 (which is the code returned by an installed WSL)
	if ($LASTEXITCODE -ne -1) {
		wsl --install
	} else {
		Write-Host "WSL already installed." -ForegroundColor DarkRed
	}
}


Write-Host "Installing Tools" -ForegroundColor DarkGreen
Write-Host "Installing Arc..." -ForegroundColor DarkYellow
installIfNotInstalled "TheBrowserCompany.Arc"
Write-Host "Installing WinRAR..." -ForegroundColor DarkYellow
installIfNotInstalled "RARLab.WinRAR" "winrar"
Write-Host "Installing Notepad++..." -ForegroundColor DarkYellow
installIfNotInstalled "Notepad++.Notepad++" "notepad++"
Write-Host "Installing Everything..." -ForegroundColor DarkYellow
installIfNotInstalled "voidtools.Everything" "everything"
Write-Host "Installing Powershell..." -ForegroundColor DarkYellow
installIfNotInstalled "Microsoft.PowerShell"
Write-Host "Installing OhMyPosh..." -ForegroundColor DarkYellow
installIfNotInstalled "JanDeDobbeleer.OhMyPosh"
Write-Host "Installing Gsudo..." -ForegroundColor DarkYellow
installIfNotInstalled "gerardog.gsudo" "gsudo"
Write-Host "Installing Git..." -ForegroundColor DarkYellow
installIfNotInstalled "Git.Git" "git"
Write-Host "Installing nvm..." -ForegroundColor DarkYellow
installIfNotInstalled "CoreyButler.NVMforWindows" "nodejs"
Write-Host "Installing Docker..." -ForegroundColor DarkYellow
installIfNotInstalled "Docker.DockerDesktop" "docker"
Write-Host "Installing JetBrains Toolbox..." -ForegroundColor DarkYellow
installIfNotInstalled "JetBrains.Toolbox" "toolbox"
Write-Host "Installing MariaDB..." -ForegroundColor DarkYellow
installIfNotInstalled "MariaDB.Server" "mariadb"
Write-Host "Installing DBeaver..." -ForegroundColor DarkYellow
installIfNotInstalled "dbeaver.dbeaver" "dbeaver"
Write-Host "Installing Kubectl..." -ForegroundColor DarkYellow
installIfNotInstalled "Kubernetes.kubectl" "kubectl"
Write-Host "Installing K9s..." -ForegroundColor DarkYellow
installIfNotInstalled "Derailed.k9s" "k9s"
Write-Host "Installing Discord..." -ForegroundColor DarkYellow
installIfNotInstalled "Discord.Discord" "discord"
Write-Host "Installing Bitwarden..." -ForegroundColor DarkYellow
installIfNotInstalled "Bitwarden.Bitwarden"

Write-Host "Installing WSL" -ForegroundColor DarkGreen
installWSL
